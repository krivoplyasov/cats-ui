import { expect, test } from '@playwright/test';

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({
  page,
}) => {
  await page.route('**/cats/rating', route => {
    route.fulfill({
      status: 500,
      body: 'Internal Server Error',
    });
  });
  await page.goto('/rating');
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается', async ({ page }) => {
  await page.goto('/rating');
  const likes = await page
    .locator('//table[1]/tbody/tr/td[contains(@class, "item-count")]')
    .allTextContents();
  const sortedLikes = [...likes].sort((a, b) => Number(b) - Number(a));
  expect(likes).toEqual(sortedLikes);
});
